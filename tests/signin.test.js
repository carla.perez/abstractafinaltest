import { expect } from "chai";
import { validAccount, invalidAccount } from "../data/account";
import HomePage from '../pages/home.page';
import AuthenticationPage from '../pages/authentication.page';

describe ('Sign in', () => {
    beforeEach(async function () {
        await HomePage.open('');
        await HomePage.clickSigninMenu();
    });
    
    it('Should signing in the application when clicking the sign in button', async() => {
        
        
        await AuthenticationPage.completeAuthentication(
          validAccount.email,
          validAccount.password
        );
        await AuthenticationPage.clickSigninButton();
        await browser.pause(1500);
        expect(await AuthenticationPage.pageTitle.getText()).to.equal('MY ACCOUNT');
        await HomePage.clickSignoutButton();
    });
    it('Should not signing in the application with invalid credentials when clicking  the sign in button', async() => {
        
      
        await AuthenticationPage.completeAuthentication(
          invalidAccount.email,
          invalidAccount.password
        );
        await AuthenticationPage.clickSigninButton();
        expect (await AuthenticationPage.alertMessage).to.exist;
    });
    it('Should not signing in the application with empty credentials when clicking the sign in button', async() => {
      
        await AuthenticationPage.completeAuthentication(
          '',''
        );
        await AuthenticationPage.clickSigninButton();
        expect (await AuthenticationPage.alertMessage).to.exist;
    });
    it('Should not signing in the application with valid email and invalid password when clicking the sign in button', async() => {
      
        await AuthenticationPage.completeAuthentication(
          validAccount.email,
          invalidAccount.password
        );
        await AuthenticationPage.clickSigninButton();
        expect (await AuthenticationPage.alertMessage).to.exist;
    });
    it('Should not signing in the application with invalid email and valid password when clicking the sign in button', async() => {
      
        await AuthenticationPage.completeAuthentication(
          invalidAccount.email,
          validAccount.password
        );
        await AuthenticationPage.clickSigninButton();
        expect (await AuthenticationPage.alertMessage).to.exist;
    });
    it('Should not signing in the application with valid email and empty password when clicking the sign in button', async() => {
      
        await AuthenticationPage.completeAuthentication(
          validAccount.email,
          ''
        );
        await AuthenticationPage.clickSigninButton();
        expect (await AuthenticationPage.alertMessage).to.exist;
    });
    it('Should not signing in the application with empty email and valid password when clicking the sign in button', async() => {
        
        await AuthenticationPage.completeAuthentication(
          '',
          validAccount.password
        );
        await AuthenticationPage.clickSigninButton();
        expect (await AuthenticationPage.alertMessage).to.exist;
    });
    it('Should not signing in the application with invalid email and empty password when clicking the sign in button', async() => {
        
        await AuthenticationPage.completeAuthentication(
          invalidAccount.email,
          ''
        );
        await AuthenticationPage.clickSigninButton();
        expect (await AuthenticationPage.alertMessage).to.exist;
    });
    it('Should not signing in the application with empty email and invalid password when clicking the sign in button', async() => {
        
        await AuthenticationPage.completeAuthentication(
          '',
          invalidAccount.password
        );
        await AuthenticationPage.clickSigninButton();
        expect (await AuthenticationPage.alertMessage).to.exist;
    });
});