# WebdriverIO Final Test

## Description
WebdriverIO final test for the Abstracta Academy course.
Test cases implemented for the site: [My Store](http://automationpractice.com/index.php)

## Installation
* Install [Java SDK](https://openjdk.java.net/)
* Install [Node.js](https://nodejs.org/download/release/v12.21.0/)
* Install [Visual Studio Code](https://code.visualstudio.com/download)

## Executing VSCode
* Create a new folder and add it to the workspace
* In a new terminal run: npm install 

## Executing Tests or Suites
* Execute "npm run test"
* npx wdio ./wdio.conf.js --suite nameSuite

